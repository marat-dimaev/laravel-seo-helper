<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;
use MD\LaravelSeo\Helper;

Route::prefix('seo-helper')->middleware(['web','auth','role:admin'])->group(function () {
    Route::post('get-fields', function (Request $request) {
        $response = [];

        try {
            $request->validate([
                'pageUrl' => 'required',
            ]);

            $SeoHelper = new Helper();
            $pageUrl = $request->get('pageUrl');
            $values = $SeoHelper->getDbValues($pageUrl);
            $response['status'] = 'ok';
            $response['values'] = $values;
            $response['message'] = 'Данные получены';
        } catch (\Exception $e) {
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    });
    Route::post('set-fields', function (Request $request) {
        $response = [];

        try {
            $request->validate([
                'pageUrl' => 'required',
                'fields' => 'array',
            ]);

            $SeoHelper = new Helper();
            $pageUrl = $request->get('pageUrl');
            $SeoHelper->setDbValues($pageUrl, $request->get('fields'));
            $response['status'] = 'ok';
            $response['message'] = 'Данные сохранены';
        } catch (\Exception $e) {
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    });
});