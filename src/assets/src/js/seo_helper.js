import axios from 'axios';
import Vue from 'vue';

window.axios = axios;
window.Vue = Vue;

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

Vue.component('seo-helper', require('./components/SeoHelper.vue'));
const seo_helper = new Vue({
    el: '#seo_helper'
});