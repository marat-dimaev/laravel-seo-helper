<?php
/**
 * Created by PhpStorm.
 * User: marat
 * Date: 21.03.19
 * Time: 21:01
 */

namespace MD\LaravelSeo;

use Artesaos\SEOTools\SEOTools as SEO;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class SeoHelper
 * @package MD\LaravelSeo
 *
 * @property \Artesaos\SEOTools\SEOTools $seo
 */
class Helper
{
    const TITLE = 'title';
    const DESCRIPTION = 'description';
    const KEYWORDS = 'keywords';
    const TW_TITLE = 'twitter:title';
    const TW_DESCRIPTION = 'twitter:description';
    const TW_URL = 'twitter:url';
    const TW_IMAGE = 'twitter:image';
    const OG_TITLE = 'opengraph:title';
    const OG_DESCRIPTION = 'opengraph:description';
    const OG_URL = 'opengraph:url';
    const OG_IMAGE = 'opengraph:image';
    protected static $defaults = [];
    protected $seo;

    public function __construct()
    {
        $this->seo = new SEO();
    }

    public static function generateMeta()
    {
        $SeoHelper = new self();
        $SeoHelper->loadValues();
        $html = \Artesaos\SEOTools\Facades\SEOTools::generate();
        return $html;
    }

    public static function getDefaults() {
        $pageUrl = url(self::getPageUrl());//request()->fullUrl();
        if (isset(self::$defaults['title']) && !isset(self::$defaults['twitter:title'])) {
            self::setDefault('twitter:title', self::$defaults['title']->value);
        }
        if (isset(self::$defaults['title']) && !isset(self::$defaults['opengraph:title'])) {
            self::setDefault('opengraph:title', self::$defaults['title']->value);
        }
        if (!isset(self::$defaults['twitter:url'])) {
            self::setDefault('twitter:url', $pageUrl);
        }
        if (!isset(self::$defaults['opengraph:url'])) {
            self::setDefault('opengraph:url', $pageUrl);
        }

        return self::$defaults;
    }

    protected function loadValues($pageUrl = false)
    {
        $fields = $this->getDbValues($pageUrl);
        $defaults = self::getDefaults($pageUrl);

        $fields = array_merge($defaults, $fields);

        // base default
        foreach ($fields as $field) {
            if (!$field->value) continue;

            switch ($field->code) {
                // meta
                case self::TITLE:
                    $this->seo->setTitle($field->value);
                    break;
                case self::DESCRIPTION:
                    $this->seo->setDescription($field->value);
                    break;
                case self::KEYWORDS:
                    $this->seo->metatags()->setKeywords($field->value);
                    break;
            }
        }

        // additional
        foreach ($fields as $field) {
            if (!$field->value) continue;

            switch ($field->code) {
                // twitter
                case self::TW_TITLE:
                    $this->seo->twitter()->setTitle($field->value);
                    break;
                case self::TW_DESCRIPTION:
                    $this->seo->twitter()->setDescription($field->value);
                    break;
                case self::TW_URL:
                    $this->seo->twitter()->setUrl($field->value);
                    break;
                case self::TW_IMAGE:
                    $this->seo->twitter()->addImage($field->value);
                    break;

                // opengraph
                case self::OG_TITLE:
                    $this->seo->opengraph()->setTitle($field->value);
                    break;
                case self::OG_DESCRIPTION:
                    $this->seo->opengraph()->setDescription($field->value);
                    break;
                case self::OG_URL:
                    $this->seo->opengraph()->setUrl($field->value);
                    break;
                case self::OG_IMAGE:
                    $this->seo->opengraph()->addImage($field->value);
                    break;
            }
        }
    }

    public function getDbValues($pageUrl = false)
    {
        if (!$pageUrl) $pageUrl = self::getPageUrl();
        $fields = DB::table('seo_helper_values')
            ->select('id', 'code', 'value')
            ->where('page_url', $pageUrl)
            ->get()->all();
        $newArr = [];
        foreach ($fields as $field) {
            $newArr[$field->code] = $field;
        }
        return $newArr;
    }

    public static function getPageUrl()
    {
        return request()->getPathInfo();
    }

    public static function setDefault($code, $value)
    {
        self::$defaults[$code] = (object)[
            'code'  => $code,
            'value' => $value,
        ];
    }

    public static function generateScrips()
    {
        if (self::includeAssets()) {
            return '<div id="seo_helper"><seo-helper page-url="' . self::getPageUrl() . '" :default-values="' . htmlspecialchars(json_encode(self::getDefaults())) . '"></seo-helper></div>
            <script src="' . asset('vendor/laravel-seo-helper/seo_helper.js') . '" type="text/javascript"></script>';
        }
    }

    protected static function includeAssets()
    {
        $user = Auth::user();
        return $user && $user->hasRole('admin');
    }

    public function setDbValues($pageUrl, $fields)
    {
        $existValues = $this->getDbValues($pageUrl);

        $insertValues = [];

        foreach ($fields as $code => $value) {
            if (isset($existValues[$code])) {
                $id = $existValues[$code]->id;
                DB::table('seo_helper_values')->where('id', $id)->update(['value' => $value['value']]);
            } else {
                $insertValues[] = [
                    'page_url' => $pageUrl,
                    'code'     => $code,
                    'value'    => $value['value'],
                ];
            }
        }

        if (!empty($insertValues)) {
            DB::table('seo_helper_values')->insert($insertValues);
        }
    }
}