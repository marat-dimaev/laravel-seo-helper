<?php
/**
 * Created by PhpStorm.
 * User: marat
 * Date: 21.03.19
 * Time: 21:29
 */

namespace MD\LaravelSeo;


class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/migrations');
        $this->publishes([
            __DIR__.'/assets/build/js/seo_helper.js' => public_path('vendor/laravel-seo-helper/seo_helper.js'),
        ]);
        $this->loadRoutesFrom(__DIR__.'/routes.php');
    }
}