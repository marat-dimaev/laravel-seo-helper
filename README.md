# Install

```
"require": {
    "md/laravel-seo-helper": "dev-master"
},
"repositories": [
    {
        "type": "vcs",
        "url": "git@bitbucket.org:marat-dimaev/laravel-seo-helper.git"
    }
]
```

`composer update`

`php artisan migrate`

`php artisan vendor:publish --provider="Artesaos\SEOTools\Providers\SEOToolsServiceProvider"`

`php artisan vendor:publish --provider="MD\LaravelSeo\ServiceProvider"`

# Use

1. paste in <head\> `{!! \MD\LaravelSeo\Helper::generateMeta() !!}`
2. paste before <body\> close tag `{!! \MD\LaravelSeo\Helper::generateScrips() !!}`
3. optional set default values `use MD\LaravelSeo\Helper as SeoHelper;` and `SeoHelper::setDefault(SeoHelper::TITLE, 'This is title');`

# Available SEO fields

```
const TITLE = 'title';
const DESCRIPTION = 'description';
const KEYWORDS = 'keywords';
const TW_TITLE = 'twitter:title';
const TW_DESCRIPTION = 'twitter:description';
const TW_URL = 'twitter:url';
const TW_IMAGE = 'twitter:image';
const OG_TITLE = 'opengraph:title';
const OG_DESCRIPTION = 'opengraph:description';
const OG_URL = 'opengraph:url';
const OG_IMAGE = 'opengraph:image';
```